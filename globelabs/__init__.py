import os
import errno
import logging

from six.moves import configparser

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Config:
    def __init__(self, **entries):
        self.__dict__.update(entries)

globelabs_settings = None

try:
    logger.info("Trying standalone configuration")
    # import local config
    parser = configparser.ConfigParser()
    parser.optionxform = str
    config = None
    for loc in os.getcwd(), os.path.expanduser("~"), os.environ.get("GLOBELABS_CONF"):
        source = os.path.join(loc, "globelabs.ini")
        logger.info('Trying INI Path: {}'.format(source))
        if os.path.isfile(source) and os.access(source, os.R_OK):
            logger.info("File exists and is readable")
        else:
            logger.warning("File not found: {}".format(source))
            continue
        parser.read(source)
        if not parser.has_option('DEFAULT','GLOBELABS_HOST_URL'):
            logger.warning('Missing Globelabs options')
            continue
        config = dict(parser.items('DEFAULT'))
        config['USING_DJANGO'] = False

        globelabs_settings = Config(**config)

        logger.debug('using globelabs.ini in {}'.format(source))
        break
    if not globelabs_settings:
        raise TypeError('File not found')
except(TypeError, Exception) as err:
    logger.warning('No default configuration found, Trying Django configuration')

    # import django settings
    try:
        from django.conf import settings
        if not hasattr(settings, 'GLOBELABS_HOST_URL'):
            raise KeyError('GLOBELABS Configurations are not defined')

        globelabs_settings = settings
        globelabs_settings.USING_DJANGO = True
        logger.info('Got settings from django.conf')
    except ImportError as e:
        logger.error(e)
        logger.error('No Globelabs settings found')

from globelabs.account import Account

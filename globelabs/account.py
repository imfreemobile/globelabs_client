from .core import GlobelabsAPI

class Account(GlobelabsAPI):

    def __init__(self, settings=None):
        super(Account, self).__init__(settings)

    def get_wallet_balance(self):
        return super(Account, self).get_wallet_balance()

# import os
# import binascii
import requests
import time
import logging

from decimal import Decimal

from six.moves.urllib.parse import urlparse
from base64 import b64encode

# from . import elp_settings
from bs4 import BeautifulSoup
from .loggers import LogFileHandler, ELPTXLogger
from .helpers import (QueryResult, DictToObj, create_token, send_request, call_remote, generate_reference_code)
from .constants import respCodes
from .exceptions import ELPException

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

txlogger = ELPTXLogger.setup_logger()


class GlobelabsAPI(object):

    # todo: add other APIs for sending SMS, charging, voice, location, etc...

    def __init__(self, settings=None):
        if settings is None:
            raise ValueError('Globelabs settings not found')

        try:
            if isinstance(settings, dict):
                print('converting dict to object')
                self._config = DictToObj(settings)
            else:
                print('passing object as is')
                self._config = settings
            getattr(self._config, 'GLOBELABS_HOST_URL')   
        except(AttributeError, Exception) as err:
            raise Exception('Invalid settings: %s' % err)          

        # self._config = elp_settings
        logger.info(self._config)
        o = urlparse(self._config.GLOBELABS_HOST_URL)
        self._host_url = o.netloc.split(':')[0]
        self._user = self._config.GLOBELABS_USER
        self._password = self._config.GLOBELABS_PASSWORD

    def _get_timestamp(self):
        """
        :return: timestamp
        """
        return int(round(time.time()))

    def _get_reference_number(self):
        """
        :return: randomized 16 character string
        """
        return generate_reference_code()

    def _header_setup(self, **kwargs):
        """
        :param **kwards:
        :return: header content
        """

        headers = {
            'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Accept-Encoding' : 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.9',
            'Cache-Control' : 'max-age=0',
            'Connection' : 'keep-alive',
            'Content-Type' : 'application/x-www-form-urlencoded',
            'Host' : self._host_url,
            'Origin' : 'https://%s' % self._host_url,
            'Referer' : 'https://%s/users/login' % self._host_url,
            'User-Agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:42.0) Gecko/20100101 Firefox/42.0'
        }

        headers.update(kwargs)

        return headers

    def _create_request(self, endpoint, payload, headers):
        logger.info("Preparing request parameters...")
        req = requests.Request('POST', url=endpoint, data=payload, headers=headers).prepare()
        return req

    def __extract_csrf_token(self, cookies):
        """
        Get the csrf token from the cookie set in the response headers
        """
        cookie_dict = {}

        for cookie in cookies:
            temp = cookie.split('=')
            cookie_dict[temp[0]] = temp[1]

        return cookie_dict['csrftoken']

    def __extract_csrf_middleware_token(self, content):
        """
        Get the assigned csrf middleware token for the current session form the html page
        """

        csrf_middleware_token = ''
        soup = BeautifulSoup(content, 'html.parser')
        for input in (soup.findAll('input')):
            if input['type'] in ('hidden'):
                value = ''
                if input.has_attr('value'):
                    if input['name'] == 'csrfmiddlewaretoken':
                        value = input['value']
                        csrf_middleware_token = value
                        break

        return csrf_middleware_token

    def __get_csrf_tokens(self):
        """
        Get the csrf tokens
        """
      
        url = 'https://%s/users/login' % self._host_url

        headers = self._header_setup()

        page = requests.get(url, headers=headers)
        headers = page.headers
            
        if 'Set-Cookie' in headers:
            cookie = headers['Set-Cookie']  if 'Set-Cookie' in headers else ''
            cookie_data = cookie.split(';')

        csrf_token = self.__extract_csrf_token(cookie_data)
        csrf_middleware_token = self.__extract_csrf_middleware_token(page.content)
        
        tokens = {
            'csrf_token' : csrf_token,
            'csrf_middleware_token' : csrf_middleware_token
        }
        return tokens

    def get_wallet_balance(self):
        csrf_tokens = self.__get_csrf_tokens()

        url = 'https://%s/users/login' % self._host_url

        cookies = {'Cookie' : 'csrftoken=%s' % csrf_tokens['csrf_token']}
        headers = self._header_setup(**cookies)

        data = {
            'csrfmiddlewaretoken' : '%s' % csrf_tokens['csrf_middleware_token'],
            'email' : self._user,
            'password' : self._password
        }

        page = requests.post(url, data=data, headers=headers)
        soup = BeautifulSoup(page.content, 'html.parser')
        
        balance = "Unable to parse wallet balance"

        try:
            for label in soup.findAll('label', class_='lbl-wallet-amount'):
                balance = label.text
                return balance
        except:
            logger.error('Unable to parse wallet balance')
